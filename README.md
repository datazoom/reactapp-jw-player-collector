# React + Vite JW Player Collector App

Welcome to your React + JW Player Collector application! This project is set up with Create React App to build a user interface that interacts with JW Player and collects relevant data.

## Getting Started

Follow these instructions to get the project up and running on your local machine.

### Prerequisites

Make sure you have Node.js and npm installed on your machine. This project requires Node.js version 16 or above.

- Node.js: [Download here](https://nodejs.org/)
- npm: Included with Node.js installation

### Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/datazoom/reactapp-jw-player-collector.git

2. Navigate to the project directory:   
   ```
    cd reactapp-jw-player-collector
   ```
4. Set the following environment variables (Plece enter the JW script & config ID accordingly):

   Windows (Command Prompt):
   ```
   set VITE_JW_SCRIPT=<JW SCRIPT>
   set VITE_DZ_CONFIG_ID=<CONFIG ID>
   ```
   Windows (PowerShell):
   ```
   $env:VITE_JW_SCRIPT = <JW SCRIPT>
   $env:VITE_DZ_CONFIG_ID = "<CONFIG ID>"
   ``` 
   Linux / macOS:
   ```
   export VITE_JW_SCRIPT=<JW SCRIPT>
   export VITE_DZ_CONFIG_ID="<CONFIG ID>"
   ```
5. Run the command
   ```
   npm install
   ```
6. To start the development server, run
   ```
   npm run dev
   ```
   This will open your app in development mode. Open http://localhost:3000 in your browser to view it. 

## Project Structure

- **src:** Source code for your React components and application logic.
- **public:** Static assets like images, fonts, etc.
- **dist:** Build files.
