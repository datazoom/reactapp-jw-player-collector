import { useState } from "react";
import JWPlayer from "@jwplayer/jwplayer-react";
import datazoom from "@datazoom/collector_jw";
import { dzConfigId, jwScript } from "./constants";

datazoom.init({
  configuration_id: dzConfigId,
});

function App() {
  const [datazoomContext, setDatazoomContext] = useState(null);
  const [playerContext, setPlayerContext] = useState(null);

  const handleDidMount = ({ player }) => {
    let datazoomContext;
    datazoomContext = datazoom.createContext(player);
    setPlayerContext(player);
    setDatazoomContext(datazoomContext);
  };

  const handleDestory = () => {
    if (playerContext !== undefined) {
      playerContext.remove();
      setPlayerContext(null);
    }
    if (datazoomContext !== undefined) {
      datazoomContext.destroy();
      setDatazoomContext(null);
    }
  };

  return (
    <>
      <main>
        <h1>JW Player NPM</h1>
        <div
          style={{
            width: "600px",
          }}
        >
          <JWPlayer
            style={{}}
            file="https://demo.unified-streaming.com/k8s/features/stable/video/tears-of-steel/tears-of-steel.ism/.m3u8"
            library={jwScript}
            didMountCallback={handleDidMount}
          />
        </div>
        <button onClick={handleDestory}>Destroy</button>
      </main>
    </>
  );
}

export default App;
